﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeCreator : MonoBehaviour
{
    [SerializeField]
    private Mesh mesh;
    [SerializeField]
    private GameObject cubePrefab;
    private int xIndex = 10;
    private int yIndex = 50;
    private int zIndex = 10;

    void Start()
    {
        //CreateMeshForm();
        CreateCubicForm();
    }

    public void Restart()
    {
        SceneManager.LoadScene("5000Cubes");
    }

    private void CreateCubicForm()
    {
        for (int z = 0; z < zIndex; z++)
        {
            for (int x = 0; x < xIndex; x++)
            {
                for (int y = 0; y < yIndex; y++)
                {
                    Instantiate(cubePrefab, new Vector3(x, y, z), Quaternion.identity);
                }
            }
        }
    }

    private void CreateMeshForm()
    {
        Vector3[] verts = mesh.vertices;
        for (int i = 0; i < verts.Length; i++)
        {
            Instantiate(cubePrefab, verts[i], Quaternion.identity);
        }
    }
}
